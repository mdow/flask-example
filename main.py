"""
    Import the dependencies we need to run the app.
"""
# We'll be using python-dotenv to load environment variables from our .env file -- What's a .env file and why is it important?
from dotenv import load_dotenv

#  We'll need os from the Python standard library to read environment variables -- What does os stand for? What is an environment variable?
import os

# Flask is of course our server framework, and we'll also need some utilities to use in our routes -- Will I need url_for? The PluralSight guy imported that and I don't know what it is.
from flask import Flask, render_template, request

# Records is our database library -- Will I need this even though I'm using a server, not a local db?
import records

# Load environment variables from './.env' into our environment
load_dotenv()

# initialize the Flask app
app = Flask(__name__)

# fetch the DATABASE_URL environment variable -- What is DATABASE_URL? Is it the location of your local db? Will the process be the same for a server?
DATABASE_URL = os.getenv("DATABASE_URL")

# initialize our database connection -- What will it look like with a db that needs credentials to access? Server creds and MSSQL creds?
db = records.Database(DATABASE_URL)

# this function uses our database connection to query all columns ('*') from everything in the table 'chips' -- What does "query all columns" mean? 
def get_all_chips():
    return db.query("select * from chips;")


# this function will search our chips table for rows with a name matching the query -- Since our query was all the stuff, does it search all of the rows?
# the 'wildcard' parameter here defualts to True if it is not provided -- I don't understand this part.
def search_chips(query, wildcard=True):
    if wildcard:
        # if wildcard is true, add wildcards to the ends of the query string -- Why do we want wildcards at the end of the query string? Yeah, I'm completely lost.
        query = "%" + query + "%"
    # run our search query, uppercasing both the names in the database and our query so our search is case-insensitive
    # Records fills in :name with the what was given to the name= parameter
    return db.query(
        "select * from chips where upper(name) LIKE :name;", name=query.upper() 
    )
    # Note: there are better ways to do case-insensitivity in Postgres, but we're trying to be database-agnostic here


# decorate the following function to be a route at "/" in our Flask server
@app.route("/")
def chips_index():
    # initialize our chips variable with an empty list
    chips = []

    # attempt to pull a URL parameter named 'query' from the current request
    query = request.args.get("query")

    # if we found a query in the URL
    if query:
        # set chips to be a search result
        chips = search_chips(query)
    else:
        # otherwise, just get all of them
        chips = get_all_chips()

    # render our index template, passing it our chips variable
    return render_template("index.html", chips=chips)

