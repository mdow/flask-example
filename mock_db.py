"""
  This script exists to set up a database with mock data for our example.
"""

from dotenv import load_dotenv
import os
import records

load_dotenv()

chips = (
    ("Doritos", 3),
    ("Cheetos", 5),
    ("Pringles", 4),
    ("Tostitos", 2),
    ("Lays", 3),
    ("Ruffles", 5),
)


class MockDbSetup:
    def __init__(self, db_url):
        self.db = records.Database(db_url)
        self.setup()

    def setup(self):
        self.setup_table()
        for (name, rating) in chips:
            self.create_chip(name, rating)

    def setup_table(self):
        self.db.query(
            """
          DROP TABLE IF EXISTS "chips";
          CREATE TABLE "chips" ("id" serial,"name" text,"rating" int2, PRIMARY KEY (id));
      """
        )

    def create_chip(self, name, rating):
        self.db.query(
            "INSERT INTO chips (name, rating) VALUES (:name, :rating);",
            name=name,
            rating=rating,
        )


MockDbSetup(os.getenv("DATABASE_URL"))
